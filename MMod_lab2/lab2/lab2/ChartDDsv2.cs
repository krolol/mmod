﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
	public partial class ChartDDsv2 : Form
	{
		private readonly List<double> _numbers;
		private readonly List<double> _probabilityX;
		private readonly List<double> _probabilityY;

		public ChartDDsv2(List<double> numbers, List<double> probabilityX, List<double> probabilityY)
		{
			InitializeComponent();
			_numbers = numbers;
			_probabilityX = probabilityX;
			_probabilityY = probabilityY;
			BindData();
		}

		private void BindData()
		{
			chart1.Series[0].Points.DataBindXY(_numbers, _probabilityX);
			chart1.Series[1].Points.DataBindXY(_numbers, _probabilityY);
		}
	}
}
