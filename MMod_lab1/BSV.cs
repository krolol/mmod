﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;
using MathNet.Numerics;

namespace MMod_lab1
{
	public class Bsv
	{
		public readonly List<double> Numbers;
		private const double MaxNumber = 100000000;
		public readonly int Min;
		public readonly int Max;
		public readonly int Count;
		private readonly Random _random;
		private List<double> _numbers;

		public Bsv(int min, int max, int count)
		{
			Min = min;
			Max = max;
			Count = count;
			_random = new Random();
			Numbers = new List<double>();
			_numbers = new List<double>();
		}

		public List<double> Generate()
		{
			Numbers.Add(_random.Next(0, 100000000));
			for (int i = 1; i < Count; i++)
			{
				Numbers.Add(Math.Pow(Numbers[i - 1], 2));
				Numbers[i] = Math.Floor(Numbers[i] / 10000);
				Numbers[i] %= 100000000;
			}

			for (int i = 0; i < Count; i++)
			{
				_numbers.Add(Numbers[i]/100000000.0);
			}

			return ConvertToInterval();
		}

		private List<double> ConvertToInterval()
		{
			double step = MaxNumber / (Max + 1 - Min);
			for (int i = 0; i < Count; i++)
			{
				Numbers[i] = Math.Floor(Numbers[i] / step);
				Numbers[i] += Min;
			}
			return Numbers;
		}

		public List<int> DrawChart()
		{
			List<int> counts = new List<int>();
			for (int i = 0; i < Max+1-Min; i++)
				counts.Add(0);
			foreach (var number in Numbers)
			{
				counts[(int)number - Min]++;
			}
			return counts;
		}

		public Dictionary<string, double> Correllation()
		{
			Dictionary<string, double> correllation = new Dictionary<string, double>();
			int s = 1;
			double sum = 0;
			for (int i = 0; i < Count - s; i++)
			{
				sum += _numbers[i] * _numbers[i + s];
			}
			double correllationS = 12 * sum / (Count - s) - 3;
			correllation.Add("1", correllationS);
			s = 2;
			sum = 0;
			for (int i = 0; i < Count - s; i++)
			{
				sum += _numbers[i] * _numbers[i + s];
			}
			correllationS = 12 * sum / (Count - s) - 3;
			correllation.Add("2", correllationS);
			return correllation;
		}

		public double XiSquare()
		{
			double Xi = 0;
			for (double i = 0.1, k = 0; i <= 1; i += 0.1, k+= 0.1)
			{
				double left = k;
				double right = i;
				double pTheority = right - left;
				double count = 0;
				for (int j = 0; j < Count; j++)
				{
					if (_numbers[j] >= left & _numbers[j] < right)
					{
						count++;
					}
				}
				double pOur = count / Count;
				Xi += Math.Pow(pTheority - pOur, 2) / pTheority;
			}
			Xi *= Count;
			return Xi;
		}
	}
}
