﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MMod_lab1
{
	public partial class Form1 : Form
	{
		private Bsv _bsv;
		private Nsv _nsv;
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			int.TryParse(textBox4.Text, out int min);
			int.TryParse(textBox3.Text, out int max);
			int.TryParse(textBox1.Text, out int count);
			_bsv = new Bsv(min, max, count);
			listBox1.DataSource = _bsv.Generate();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			List<int> counts = _bsv.DrawChart();
			List<double> numbers = new List<double>();
			for (int i = 0; i < _bsv.Max + 1 -_bsv.Min; i++)
				numbers.Add(_bsv.Min + i);
			Marks marks = new Marks(_bsv.Count, _bsv.Numbers);
			double expectation = marks.MathExpect();
			double dispersion = marks.SelectiveDispersion(expectation);
			double correctDispersion = marks.CorrectedSelectiveDispersion(dispersion);
			double leftExpectation = marks.IntervalForExpectation(expectation, correctDispersion)["left"];
			double rightExpectation = marks.IntervalForExpectation(expectation, correctDispersion)["right"];
			double leftDispersion = marks.IntervalForDispersion(correctDispersion)["left"];
			double rightDispersion = marks.IntervalForDispersion(correctDispersion)["right"];
			double xiSquare = Math.Round(_bsv.XiSquare(), 4);
			Chart chart = new Chart(numbers, counts, expectation, dispersion, correctDispersion, leftExpectation, rightExpectation, leftDispersion, rightDispersion);
			chart.Correllation = _bsv.Correllation();
			chart.XiSquare = xiSquare;
			chart.TableXiSquare = Math.Round(MathNet.Numerics.Distributions.ChiSquared.InvCDF(10 - 1, 0.95), 4);
			chart.Show();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			int.TryParse(textBox2.Text, out int count);
			_nsv = new Nsv(count);
			listBox2.DataSource = _nsv.Generate();

		}

		private void button4_Click(object sender, EventArgs e)
		{
			List<int> counts = _nsv.DrawChart();
			List<double> numbers = new List<double>();
			for (double i = -5.6; i < 6.4; i += 0.4)
				numbers.Add(i);
			Marks marks = new Marks(_nsv.Count, _nsv.Numbers);
			double expectation = marks.MathExpect();
			double dispersion = marks.SelectiveDispersion(expectation);
			double correctDispersion = marks.CorrectedSelectiveDispersion(dispersion);
			double leftExpectation = marks.IntervalForExpectation(expectation, correctDispersion)["left"];
			double rightExpectation = marks.IntervalForExpectation(expectation, correctDispersion)["right"];
			double leftDispersion = marks.IntervalForDispersion(correctDispersion)["left"];
			double rightDispersion = marks.IntervalForDispersion(correctDispersion)["right"];
			Chart chart = new Chart(numbers, counts, expectation, dispersion, correctDispersion, leftExpectation, rightExpectation, leftDispersion, rightDispersion);
			chart.Kolmogorov = _nsv.Kolmogorov();
			chart.ShowDialog();
		}
	}
}
