﻿using System;
using System.Collections.Generic;
using System.Security.Policy;

// ReSharper disable All

namespace lab2
{
	class Dsv
	{
		private List<double> _dsvList;
		private List<(double, double)> _dDsvTuple;
		private readonly int _count;
		private readonly int _countSection;
		public List<double> NotConvertedNumbersXY;
		public List<double> Numbers;
		public List<(double, double)> NumbersXY;
		public List<double> Fx;
		public List<double> Fxy;

		public Dsv(int count, int countSection)
		{
			_count = count;
			_countSection = countSection;
		}

		public List<double> Generate(Func<double, Dictionary<string, double>, double> distribution, Dictionary<string, double> args)
		{
			double p = 0;
			Fx = new List<double>();
			for (int i = 0; i < _countSection; i++)
			{
				p += distribution(i, args);
				Fx.Add(p);
			}
			Numbers = ConvertFromBsv(Fx);
			return Numbers;
		}

		public List<(double, double)> GenerateDoubleDsv(Func<int, int, double> distribution)
		{
			Fxy = new List<double>();
			double pXY = 0;
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
				{
					pXY += distribution(i, j);
					Fxy.Add(pXY);
				}
			}

			return NumbersXY = ConvertFromBsvToDDsv(Fxy);
		}

		public List<double> ConvertFromBsv(List<double> f)
		{
			_dsvList = new List<double>();
			foreach (var number in Help.GenerateBsv(_count))
			{
				for (int i = 0; i < f.Count; i++)
				{
					if (number < f[i])
					{
						_dsvList.Add(i);
						break;
					}
				}
			}

			return _dsvList;
		}

		public List<(double,double)> ConvertFromBsvToDDsv(List<double> fxy)
		{
			_dDsvTuple = new List<(double, double)>();
			NotConvertedNumbersXY = new List<double>();
			foreach (var number in Help.GenerateBsv(_count))
			{
				for (int i = 0; i < fxy.Count; i++)
				{
					if (number < fxy[i])
					{
						NotConvertedNumbersXY.Add(i);
						_dDsvTuple.Add((i / 5, i % 5));
						break;
					}
				}
			}

			return _dDsvTuple;
		}

		public List<double> GetProbability(List<double> numbers, int count)
		{
			List<double> probability = new List<double>();
			for (int i = 0; i < count + 1; i++)
				probability.Add(0);
			foreach (var number in numbers)
			{
				probability[(int)number]++;
			}
			for (int i = 0; i < probability.Count; i++)
				probability[i] /= numbers.Count;
			return probability;
		}

		public List<List<double>> GetProbabilityForDDSV()
		{
			List<List<double>> probabilityMatrix = new List<List<double>>();
			for (int i = 0; i < 5; i++)
			{
				probabilityMatrix.Add(new List<double>());
				for (int j = 0; j < 5; j++)
					probabilityMatrix[i].Add(0);
			}

			foreach (var number in NotConvertedNumbersXY)
			{
				probabilityMatrix[(int)number / 5][(int)number % 5]++;
			}

			for (int i = 0; i < probabilityMatrix.Count; i++)
				for (int j = 0; j < probabilityMatrix[i].Count; j++)
					probabilityMatrix[i][j] /= NotConvertedNumbersXY.Count;
			return probabilityMatrix;
		}
	}
}
