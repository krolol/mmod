﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml;

// ReSharper disable All

namespace lab2
{
	class Help
	{
		public static List<double> GenerateBsv(int count)
		{
			Random rand = new Random();
			List<double> bsv = new List<double>();

			for (int i = 0; i < count; i++)
				bsv.Add(rand.NextDouble());

			return bsv;
		}

		public static double XiSquare(List<double> numbers, int count, List<double> theoretical)
		{
			double Xi = 0;
			double probTheoretical = 0;
			while (theoretical.Count % 3 != 0)
			{
				theoretical.Add(theoretical[theoretical.Count - 1]);
			}

			for (int i = 2; i < theoretical.Count; i += 3)
			{
				if (i == 2)
					probTheoretical = theoretical[i];
				else
					probTheoretical = theoretical[i] - theoretical[i - 3];

				if (probTheoretical == 0)
					continue;

				double left = i - 2;
				double right = i;
				double realCount = 0;
				for (int j = 0; j < count; j++)
				{
					if (numbers[j] >= left & numbers[j] <= right)
					{
						realCount++;
					}
				}

				double probReal = realCount / count;
				Xi += Math.Pow(probTheoretical - probReal, 2) / probTheoretical;
			}

			Xi *= count;
			return Xi;
		}

		public static double ZExpectation(List<double> realProbability, List<double> thoreticalProbability,
			double realDispersion, double theoreticalDispersion,
			double count)
		{
			double realAverage = 0;
			double theoreticalAverage = 0;
			for (int i = 0; i < realProbability.Count; i++)
			{
				realAverage += realProbability[i] * i;
				theoreticalAverage += thoreticalProbability[i] * i;
			}

			double temp = realAverage - theoreticalAverage;
			double temp2 = Math.Sqrt((realDispersion / count) + (theoreticalDispersion / count));

			return temp / temp2;
		}

		public static double FDispersion(double realDispersion, double theoreticalDispersion) =>
			realDispersion / theoreticalDispersion;

		public static Dictionary<string, List<double>> GetProbability(List<List<double>> matrix)
		{
			List<double> probabilityX = new List<double>();
			List<double> probabilityY = new List<double>();
			Dictionary<string, List<double>> probabilities = new Dictionary<string, List<double>>();
			for (int i = 0; i < matrix.Count; i++)
			{
				double pX = 0;
				double pY = 0;
				for (int j = 0; j < matrix[i].Count; j++)
				{
					pX += matrix[i][j];
					pY += matrix[j][i];
				}

				probabilityX.Add(pX);
				probabilityY.Add(pY);
			}
			probabilities.Add("X", probabilityX);
			probabilities.Add("Y", probabilityY);

			return probabilities;
		}
	}
}
