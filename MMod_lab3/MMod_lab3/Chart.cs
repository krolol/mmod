﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMod_lab3
{
	public partial class Chart : Form
	{
		private readonly List<double> _timeList;
		private readonly List<double> _probabilityRejectionList;
		private readonly List<double> _inServiceList;
		private readonly List<double> _inQueueList;
		private readonly List<double> _commonCountList;
		private readonly List<double> _servicedList;
		private readonly List<double> _notServicedList;

		public Chart(List<double> timeList, List<double> probabilityRejectionList, List<double> inServiceList,
			List<double> inQueueList, List<double> commonCountList, List<double> servicedList,
			List<double> notServicedList)
		{
			InitializeComponent();
			_timeList = timeList;
			_timeList.RemoveAt(0);
			_probabilityRejectionList = probabilityRejectionList;
			_probabilityRejectionList.RemoveAt(0);
			_inServiceList = inServiceList;
			_commonCountList = commonCountList;
			_inQueueList = inQueueList;
			_notServicedList = notServicedList;
			_servicedList = servicedList;
			BindData();
		}

		private void BindData()
		{
			chart1.Series[0].Points.DataBindXY(_timeList, _probabilityRejectionList);
			chart2.Series[0].Points.DataBindXY(_timeList, _commonCountList);
			chart2.Series[1].Points.DataBindXY(_timeList, _inQueueList);
			chart2.Series[2].Points.DataBindXY(_timeList, _inServiceList);
			chart2.Series[3].Points.DataBindXY(_timeList, _servicedList);
			chart2.Series[4].Points.DataBindXY(_timeList, _notServicedList);
		}
	}
}
