﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMod_lab4
{
	public partial class Charts : Form
	{
		private readonly Dictionary<double, double> _model;
		private readonly Dictionary<double, double> _realCorrelation;
		private readonly Dictionary<double, double> _tCorrelation;
		private readonly Dictionary<double, double> _portrait;

		public Charts(Dictionary<double, double> model, Dictionary<double, double> realCorrelation, Dictionary<double, double> tCorrelation, Dictionary<double, double> portrait)
		{
			InitializeComponent();
			_model = model;
			_realCorrelation = realCorrelation;
			_tCorrelation = tCorrelation;
			_portrait = portrait;
			BindData();
		}

		public void BindData()
		{
			chart1.Series[0].ChartType = SeriesChartType.FastLine;
			List<double> keys = _model.Keys.ToList();
			List<double> values = _model.Values.ToList();
			chart1.Series[0].Points.DataBindXY(keys, values);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			chart1.Series[1].ChartType = SeriesChartType.FastLine;
			chart1.Series[0].ChartType = SeriesChartType.FastLine;
			List<double> keys0 = _realCorrelation.Keys.ToList();
			List<double> values0 = _realCorrelation.Values.ToList();
			List<double> keys1 = _tCorrelation.Keys.ToList();
			List<double> values1 = _tCorrelation.Values.ToList();
			chart1.Series[0].Points.DataBindXY(keys0, values0);
			chart1.Series[1].Points.DataBindXY(keys1, values1);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			BindData();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			chart1.Series[0].ChartType = SeriesChartType.FastLine;
			List<double> keys = _portrait.Keys.ToList();
			List<double> values = _portrait.Values.ToList();
			chart1.Series[0].Points.DataBindXY(keys, values);
		}
	}
}
