﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MMod_lab3
{
	class Time
	{
		private static List<double> allTime = new List<double>();
		private static double count = 0;
		public static double GetTime(double v)
		{
			if (allTime.Count == 0)
				allTime.Add(0);
			var time = MathNet.Numerics.Distributions.Exponential.Sample(v);
			count++;
			allTime[0] += time;
			return time ;
		}

		public static double GetTimeParameter()
		{
			if (allTime.Count == 0)
				return 0;
			return 1 / (allTime[0] / count);
		}
	}
}
