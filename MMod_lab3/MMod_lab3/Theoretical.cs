﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MMod_lab3
{
	class Theoretical
	{
		public static List<double> GetFinalsProbability(int channelsCount, int queueCapacity, double requestsIntensity, double serviceIntensity, double timeParameter)
		{
			List<double> finalProbability = new List<double>();
			double loadIntensity = requestsIntensity / serviceIntensity;
			double B = timeParameter / serviceIntensity;
			double prob0 = 1;
			for (int i = 0; i < channelsCount; ++i)
			{
				prob0 += Math.Pow(loadIntensity, i + 1) / Factorial(i + 1);
			}
			double temp = 0;
			for (int i = 0; i < queueCapacity; ++i)
			{
				temp += Math.Pow(loadIntensity, i + 1) / Multiply(i + 1, channelsCount, B);
			}
			prob0 += Math.Pow(loadIntensity, channelsCount) / Factorial(channelsCount) * temp;
			prob0 = Math.Pow(prob0, -1);
			finalProbability.Add(prob0);
			for (int i = 0; i < channelsCount; i++)
			{
				double probI = Math.Pow(loadIntensity, i + 1) / Factorial(i + 1) * prob0;
				finalProbability.Add(probI);
			}
			double pn = finalProbability[finalProbability.Count - 1];
			for (int i = 0; i < queueCapacity; i++)
			{
				double probI = pn * Math.Pow(loadIntensity, i + 1) / Multiply(i + 1, channelsCount, B);
				finalProbability.Add(probI);
			}
			return finalProbability;
		}

		private static double Factorial(int number)
		{
			double result = 1;
			while (number != 1)
			{
				result = result * number;
				number = number - 1;
			}
			return result;
		}

		private static double Multiply(double i, double channelsCount, double B)
		{
			double result = 1;
			for (int j = 0; j < i; j++)
			{
				result *= (channelsCount + (j + 1) * B);
			}

			return result;
		}

		public static double GetAbsoluteBandWith(double probabilityRejection, double requestsIntensity)
		{
			double Q = 1 - probabilityRejection;
			double A = requestsIntensity * Q;
			return A;
		}

		public static double GetAVGInSmo(int channelsCount, int queueCapacity, List<double> finallyProbability)
		{
			double Ls = 0;
			for (int i = 0; i < channelsCount; i++)
			{
				Ls += (i + 1) * finallyProbability[i + 1];
			}
			for (int i = 0; i < queueCapacity; i++)
			{
				Ls += channelsCount * finallyProbability[channelsCount + i + 1];
			}
			return Ls;
		}

		public static double GetAVGInQueue(int queueCapacity, int channelsCount, List<double> finallyProbability)
		{
			double Lo = 0;
			for (int i = 0; i < queueCapacity; i++)
			{
				Lo += (i + 1) * finallyProbability[channelsCount + i + 1];
			}
			return Lo;
		}

		public static double GetAVGTimeInSmo(double probabilityRejection, double serviceIntensity, double timeParameter)
		{
			return (1 - probabilityRejection)/serviceIntensity + 1 / timeParameter;
		}

		public static double GetAVGTimeInQueue(double avgTimeInSmo, double timeParameter)
		{
			return avgTimeInSmo - 1 / timeParameter;
		}

		public static double GetAVGNotFreeChannels(double absoluteBandWith, double serviceIntensity)
		{
			return absoluteBandWith / serviceIntensity;
		}
	}
}
