﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMod_lab4
{
	class Сharacteristics
	{
		public static double MathExp(List<double> values)
		{
			double mathExp = 0;
			foreach (var value in values)
			{
				mathExp += value;
			}

			mathExp /= values.Count;
			return mathExp;
		}

		public static double Dispersion(List<double> values, double mathExp)
		{
			double dispersion = 0;
			foreach (var value in values)
			{
				dispersion += Math.Pow(value - mathExp, 2);
			}
			dispersion /= values.Count;
			return Math.Round(dispersion, 4);
		}

		public static Dictionary<double, double> TCorrelation(double mathExp, double dispersion, double alpha, double countCor, double deltaT)
		{
			Dictionary<double, double> corFunc = new Dictionary<double, double>();
			for (int i = 0; i < countCor; i++)
			{
				double value = dispersion * Math.Exp(-(alpha * i * deltaT)) * (1 - alpha * i * deltaT);
				corFunc.Add(i*deltaT, value);
			}
			return corFunc;
		}

		public static Dictionary<double, double> Portrait(double alpha, double countCor, double deltaT)
		{
			Dictionary<double, double> portrait = new Dictionary<double, double>();
			for (int i = 0; i < countCor; i++)
			{
				double value = alpha * (i * deltaT) * Math.Exp(-(alpha * i * deltaT)) * (alpha * i * deltaT -2) / (i * deltaT);
				double value2 = Math.Exp(-(alpha * i * deltaT)) * (1 - alpha * i * deltaT);
				portrait.Add(value2, value);
			}
			return portrait;
		}

		public static Dictionary<double, double> PCorrelation(List<double> values, double countCor)
		{
			Dictionary<double, double> corFunc = new Dictionary<double, double>();
			double mx = MathExp(values);
			double dx = Dispersion(values, mx);
			double cov = 0;
			for (int i = 0; i < countCor; i++)
			{
				for (int j = i + 1; j < values.Count; j++)
				{
					cov += (values[j] - mx) * (values[j - i] - mx);
				}
				cov /= values.Count;
				corFunc.Add(i, cov / dx);
			}
			return corFunc;
		}
	}
}
