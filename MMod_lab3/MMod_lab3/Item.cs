﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMod_lab3
{
	class Item
	{
		private Guid _id;
		public double TimeInQueue;

		public Item(double timeInQueue)
		{
			TimeInQueue = timeInQueue;
			_id = Guid.NewGuid();
		}

		public override string ToString()
		{
			return _id.ToString();
		}
	}
}
