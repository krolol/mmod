﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

// ReSharper disable All

namespace lab2
{
	public partial class ChartDDsv : Form
	{
		private readonly List<List<double>> _dDsvMatrix;
		private readonly double _expectationX;
		private readonly double _expectationY;
		private readonly double _dispersionX;
		private readonly double _dispersionY;
		private readonly double _corelation;
		private readonly double _realXiSquare;
		private readonly double _tableXiSquare;
		private readonly double _zTestX;
		private readonly double _zTestY;
		private readonly double _fTestX;
		private readonly double _fTestY;
		private readonly Dictionary<string, double> _intervalExpectationX;
		private readonly Dictionary<string, double> _intervalExpectationY;
		private readonly Dictionary<string, double> _intervalDispersionX;
		private readonly Dictionary<string, double> _intervalDispersionY;

		public ChartDDsv(List<List<double>> dDsvMatrix, double expectationX, double expectationY, double dispersionX,
			double dispersionY, double corelation, Dictionary<string, double> intervalExpectationX,
			Dictionary<string, double> intervalExpectationY, Dictionary<string, double> intervalDispersionX,
			Dictionary<string, double> intervalDispersionY, double realXiSquare, double tableXiSquare, double zTestX,
			double zTestY, double fTestX, double fTestY)
		{
			_dDsvMatrix = dDsvMatrix;
			_expectationX = expectationX;
			_expectationY = expectationY;
			_dispersionX = dispersionX;
			_dispersionY = dispersionY;
			_corelation = corelation;
			_realXiSquare = realXiSquare;
			_tableXiSquare = tableXiSquare;
			_intervalExpectationX = intervalExpectationX;
			_intervalExpectationY = intervalExpectationY;
			_intervalDispersionX = intervalDispersionX;
			_intervalDispersionY = intervalDispersionY;
			_zTestX = zTestX;
			_zTestY = zTestY;
			_fTestX = fTestX;
			_fTestY = fTestY;
			InitializeComponent();
			BindData();
		}

		public void BindData()
		{
			dataGridView1.ColumnCount = _dDsvMatrix.Count;
			for (int i = 0; i < _dDsvMatrix.Count; i++)
			{
				string[] row = new string[_dDsvMatrix[i].Count];
				for (int j = 0; j < _dDsvMatrix[i].Count; j++)
				{
					row[j] = _dDsvMatrix[i][j].ToString(CultureInfo.InvariantCulture);
				}

				dataGridView1.Rows.Add(row);
			}

			textBox1.Text = _expectationX.ToString(CultureInfo.InvariantCulture);
			textBox2.Text = _expectationY.ToString(CultureInfo.InvariantCulture);
			textBox3.Text = _dispersionX.ToString(CultureInfo.InvariantCulture);
			textBox4.Text = _dispersionY.ToString(CultureInfo.InvariantCulture);
			textBox5.Text = _corelation.ToString(CultureInfo.InvariantCulture);
			textBox6.Text = _intervalExpectationX["left"].ToString(CultureInfo.InvariantCulture);
			textBox7.Text = _intervalExpectationX["right"].ToString(CultureInfo.InvariantCulture);
			textBox8.Text = _intervalExpectationY["left"].ToString(CultureInfo.InvariantCulture);
			textBox9.Text = _intervalExpectationY["right"].ToString(CultureInfo.InvariantCulture);
			textBox10.Text = _intervalDispersionX["left"].ToString(CultureInfo.InvariantCulture);
			textBox11.Text = _intervalDispersionX["right"].ToString(CultureInfo.InvariantCulture);
			textBox12.Text = _intervalDispersionY["left"].ToString(CultureInfo.InvariantCulture);
			textBox13.Text = _intervalDispersionY["right"].ToString(CultureInfo.InvariantCulture);
			textBox14.Text = _realXiSquare.ToString(CultureInfo.InvariantCulture);
			textBox15.Text = _tableXiSquare.ToString(CultureInfo.InvariantCulture);
			textBox16.Text = _zTestX.ToString(CultureInfo.InvariantCulture);
			textBox17.Text = _zTestY.ToString(CultureInfo.InvariantCulture);
			textBox18.Text = _fTestX.ToString(CultureInfo.InvariantCulture);
			textBox19.Text = _fTestY.ToString(CultureInfo.InvariantCulture);
		}
	}
}