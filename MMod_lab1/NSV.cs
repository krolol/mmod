﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace MMod_lab1
{
	public class Nsv
	{
		public readonly List<double> Numbers;
		public int Count;
		private readonly Random _random;

		public Nsv(int count)
		{
			Numbers = new List<double>();
			Count = count;
			_random = new Random();
		}
		#region ИЗ ЦПД
		public List<double> Generate()
		{
			for (int i = 0; i < Count; i++)
			{
				Numbers.Add(-6);
				for (int j = 0; j < 12; j++)
				{
					Numbers[i] += _random.NextDouble();
				}
			}
			return Numbers;
		}
		#endregion

		#region Box-Muller 

		public List<double> GenerateBoxMuller()
		{
			while (Numbers.Count != Count)
			{
				double r = _random.NextDouble();
				double u = _random.NextDouble();
				double first = Math.Cos(2 * Math.PI * u) * Math.Sqrt(-2 * Math.Log10(r));
				double second = Math.Sin(2 * Math.PI * u) * Math.Sqrt(-2 * Math.Log10(r));
				Numbers.Add(first);
				if (Numbers.Count != Count)
					Numbers.Add(second);
			}
			return Numbers;
		}

		public List<double> GenerateBoxMullerVersion2()
		{
			while (Numbers.Count != Count)
			{
				double x = _random.Next(-100000000, 100000001)/100000000.0;
				double y = _random.Next(-100000000, 100000001) / 100000000.0;
				double s = Math.Pow(x, 2) + Math.Pow(y, 2);
				if (Math.Abs(s) < Math.Pow(10, -6) || s > 1) continue;
				double first = x * Math.Sqrt(-2 * Math.Log10(s) / s);
				double second = y * Math.Sqrt(-2 * Math.Log10(s) / s);
				Numbers.Add(first);
				if (Numbers.Count != Count)
					Numbers.Add(second);
			}
			return Numbers;
		}

		#endregion

		public List<int> DrawChart()
		{
			List<int> counts = new List<int>();
			for (int i = 0; i < 30; i++)
				counts.Add(0);
			foreach (var number in Numbers)
			{
				counts[(int) ((number + 6) / 0.4)]++;
			}

			return counts; 
		}

		public double Kolmogorov()
		{
			List<double> stats = new List<double>();
			for (int i = 0; i < Count; i++)
			{
				double count = 0;
				for (int j = 0; j < Count; j++)
				{
					if (Numbers[i] >= Numbers[j])
						count++;
				}
				stats.Add(count / Count);
				stats[i] -= MathNet.Numerics.Distributions.Normal.CDF(0, 1, Numbers[i]);
				stats[i] = Math.Abs(stats[i]);
			}
			double stat = Math.Sqrt(Count) * stats.Max();
			return stat;
		}
	}
}
