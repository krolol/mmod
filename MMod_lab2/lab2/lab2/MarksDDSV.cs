﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
	[SuppressMessage("ReSharper", "UseObjectOrCollectionInitializer")]
	class MarksDdsv
	{
		public List<double> ProbabilityX;
		public List<double> ProbabilityY;

		public Dictionary<string, double> GetExpectation(List<List<double>> probabilityMatrix)
		{
			ProbabilityX = new List<double>();
			ProbabilityY = new List<double>();
			GetProbability(probabilityMatrix);
			Dictionary<string, double> expectation = new Dictionary<string, double>();
			expectation.Add("X", GetExpectation(ProbabilityX));
			expectation.Add("Y", GetExpectation(ProbabilityY));
			return expectation;
		}


		private void GetProbability(List<List<double>> probabilityMatrix)
		{
			for (int i = 0; i < probabilityMatrix.Count; i++)
			{
				double pX = 0;
				double pY = 0;
				for (int j = 0; j < probabilityMatrix[i].Count; j++)
				{
					pX += probabilityMatrix[i][j];
					pY += probabilityMatrix[j][i];
				}

				ProbabilityX.Add(pX);
				ProbabilityY.Add(pY);
			}
		}

		private double GetExpectation(List<double> probability)
		{
			double expectation = 0;
			for (int i = 0; i < probability.Count; i++)
			{
				expectation += probability[i] * i;
			}

			return expectation;
		}

		public Dictionary<string, double> GetDispersion(List<List<double>> probabilityMatrix, double expectationX, double expectationY)
		{
			ProbabilityX = new List<double>();
			ProbabilityY = new List<double>();
			GetProbability(probabilityMatrix);
			Dictionary<string, double> dispersion = new Dictionary<string, double>();
			dispersion.Add("X", GetDispersion(ProbabilityX, expectationX));
			dispersion.Add("Y", GetDispersion(ProbabilityY, expectationY));
			return dispersion;
		}

		private double GetDispersion(List<double> probability, double expectation)
		{
			double dispersion = 0;
			for (int i = 0; i < probability.Count; i++)
			{
				dispersion += probability[i] * Math.Pow(i, 2);
			}

			dispersion -= Math.Pow(expectation, 2);
			return dispersion;
		}

		public double GetCorelation(double expectationX, double expectationY, double dispersionX, double dispersionY,
			List<List<double>> probabilityXY)
		{
			double expectationXY = 0;
			for (int i = 0; i < probabilityXY.Count; i++)
			for (int j = 0; j < probabilityXY[i].Count; j++)
				expectationXY += i * j * probabilityXY[i][j];
			double covariation = expectationXY - expectationX * expectationY;
			double corelation = covariation / (Math.Sqrt(dispersionX) * Math.Sqrt(dispersionY));
			return corelation;
		}

		public Dictionary<string, double> IntervalForExpectation(double expectation, double correctDispersion, double count)
		{
			Dictionary<string, double> dictionary = new Dictionary<string, double>();
			double t = 2.26;
			if (count >= 99)
			{
				t = 1.984;
			}

			if (count >= 9999)
			{
				t = 1.96;
			}

			double temp = t * Math.Sqrt(correctDispersion) / Math.Sqrt(count - 1);
			double left = Math.Round(expectation - temp, 4);
			double right = Math.Round(expectation + temp, 4);
			dictionary.Add("left", left);
			dictionary.Add("right", right);
			return dictionary;
		}

		public double CorrectedSelectiveDispersion(double dispersion, double count)
		{
			double correctDispersion = dispersion * count / (count - 1);
			return Math.Round(correctDispersion, 4);
		}

		public Dictionary<string, double> IntervalForDispersion(double correctDispersion, double count)
		{
			Dictionary<string, double> dictionary = new Dictionary<string, double>();
			double xiSquare1 = MathNet.Numerics.Distributions.ChiSquared.InvCDF(count - 1, 0.95);
			double xiSquare2 = MathNet.Numerics.Distributions.ChiSquared.InvCDF(count - 1, 0.05);
			double temp = count * correctDispersion;
			double left = Math.Round(temp / xiSquare1, 4);
			double right = Math.Round(temp / xiSquare2, 4);
			dictionary.Add("left", left);
			dictionary.Add("right", right);
			return dictionary;
		}
	}
}