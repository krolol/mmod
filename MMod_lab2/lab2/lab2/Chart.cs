﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace lab2
{
	public partial class Chart : Form
	{
		private readonly List<double> _numberList;
		private readonly List<double> _countList;
		private readonly double _expectation;
		private readonly double _selectiveDispersion;
		private readonly double _correctDispersion;
		private readonly double _realXiSquare;
		private readonly double _tableXiSquare;
		private readonly Dictionary<string, double> _intervalExpectation;
		private readonly Dictionary<string, double> _intervalDispersion;

		public Chart(List<double> numberList, List<double> countList, double expectation, double selectiveDispersion,
			double correctDispersion, Dictionary<string, double> intervalExpectation,
			Dictionary<string, double> intervalDispersion, double realXiSquare, double tableXiSquare)
		{
			InitializeComponent();
			_numberList = numberList;
			_countList = countList;
			_expectation = expectation;
			_selectiveDispersion = selectiveDispersion;
			_correctDispersion = correctDispersion;
			_intervalExpectation = intervalExpectation;
			_intervalDispersion = intervalDispersion;
			_realXiSquare = realXiSquare;
			_tableXiSquare = tableXiSquare;
			BindData();
		}

		public void BindData()
		{
			chart1.Series[0].Points.DataBindXY(_numberList, _countList);
			textBox1.Text = _expectation.ToString(CultureInfo.InvariantCulture);
			textBox2.Text = _selectiveDispersion.ToString(CultureInfo.InvariantCulture);
			textBox3.Text = _correctDispersion.ToString(CultureInfo.InvariantCulture);
			textBox4.Text = _intervalExpectation["left"].ToString(CultureInfo.InvariantCulture);
			textBox5.Text = _intervalExpectation["right"].ToString(CultureInfo.InvariantCulture);
			textBox6.Text = _intervalDispersion["left"].ToString(CultureInfo.InvariantCulture);
			textBox7.Text = _intervalDispersion["right"].ToString(CultureInfo.InvariantCulture);
			textBox8.Text = _realXiSquare.ToString(CultureInfo.InvariantCulture);
			textBox9.Text = _tableXiSquare.ToString(CultureInfo.InvariantCulture);
		}
	}
}
