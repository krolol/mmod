﻿using System;
using System.Collections.Generic;

namespace MMod_lab1
{
	class Marks
	{
		private readonly int _count;
		private readonly List<double> _numbers;

		public Marks(int count, List<double> numbers)
		{
			_count = count;
			_numbers = numbers;
		}

		public double MathExpect()
		{
			double expectation = 0;
			foreach (var number in _numbers)
			{
				expectation += number;
			}
			expectation /= _count;
			return Math.Round(expectation, 4);
		}

		public double SelectiveDispersion(double expectation)
		{
			double dispersion = 0;
			foreach (var number in _numbers)
			{
				dispersion += Math.Pow(number - expectation, 2);
			}
			dispersion /= _count;
			return Math.Round(dispersion, 4);
		}

		public double CorrectedSelectiveDispersion(double dispersion)
		{
			double correctDispersion = dispersion * _count / (_count - 1);
			return Math.Round(correctDispersion, 4);
		}

		public Dictionary<string, double> IntervalForDispersion(double correctDispersion)
		{
			Dictionary<string, double> dictionary = new Dictionary<string, double>();
			double xiSquare1 = MathNet.Numerics.Distributions.ChiSquared.InvCDF(_count - 1, 0.95);
			double xiSquare2 = MathNet.Numerics.Distributions.ChiSquared.InvCDF(_count - 1, 0.05);
			double temp = _count * correctDispersion;
			double left = Math.Round(temp / xiSquare1, 4);
			double right = Math.Round(temp / xiSquare2, 4);
			dictionary.Add("left", left);
			dictionary.Add("right", right);
			return dictionary;
		}

		public Dictionary<string, double> IntervalForExpectation(double expectation, double correctDispersion)
		{
			Dictionary<string, double> dictionary = new Dictionary<string, double>();
			double t = 2.26;
			if (_count >= 99)
			{
				t = 1.984;
			}
			if (_count >= 9999)
			{
				t = 1.96;
			}
			double temp = t * Math.Sqrt(correctDispersion) / Math.Sqrt(_count - 1);
			double left = Math.Round(expectation - temp, 4);
			double right = Math.Round(expectation + temp, 4);
			dictionary.Add("left", left);
			dictionary.Add("right", right);
			return dictionary;
		}
	}
}
