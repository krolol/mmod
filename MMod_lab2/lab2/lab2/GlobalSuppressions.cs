﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:lab2.Form1.button2_Click(System.Object,System.EventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:lab2.Form1.button1_Click(System.Object,System.EventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Стиль", "IDE0028:Упростите инициализацию коллекции", Justification = "<Ожидание>", Scope = "member", Target = "~M:lab2.MarksDDSV.GetExpectation(System.Collections.Generic.List{System.Collections.Generic.List{System.Double}})~System.Collections.Generic.Dictionary{System.String,System.Double}")]

