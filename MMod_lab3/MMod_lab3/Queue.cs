﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMod_lab3
{
	class Queue
	{
		public List<Item> QueueItems;
		public Queue(int capacity)
		{
			QueueItems = new List<Item>(capacity);
		}

		public void Add(Item item)
		{
			QueueItems.Add(item);
		}

		public void Remove(Item item)
		{
			QueueItems.Remove(item);
		}
	}
}
