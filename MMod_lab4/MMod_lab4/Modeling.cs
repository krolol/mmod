﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMod_lab4
{
	class Modeling
	{
		private readonly double _count;
		private readonly double[] _a = new double[2];
		private readonly double[] _alphaArray = new double[2];
		private readonly double[] _b = new double[2];
		public Dictionary<double, double> YnList;
		private readonly Random _random;

		public Modeling(double count, double alpha, double deltaT)
		{
			_random = new Random();
			YnList = new Dictionary<double, double>();
			_count = count;
			var gamma = alpha * deltaT;
			var p = Math.Exp(-gamma);
			_alphaArray[0] = Math.Pow(p, 3) * (1 - gamma) - p * (1 + gamma);
			_alphaArray[1] = 1 + 4 * Math.Pow(p, 2)*gamma - Math.Pow(p, 4);
			_a[0] = Math.Sqrt((Math.Pow(_alphaArray[1], 2) + Math.Sqrt(Math.Pow(_alphaArray[1], 2) - 4 * Math.Pow(_alphaArray[0], 2))) / 2);
			_a[1] = _alphaArray[0] / _alphaArray[1];
			_b[0] = 2 * p;
			_b[1] = -Math.Pow(p, 2);
		}

		public void ModelRecursive(int i, double xm, double ym, double yk, double deltaT)
		{
			double xn = 2 * (_random.Next() / (double)int.MaxValue - 0.5);
			double yn = _a[0] * xn + _a[1] * xm + _b[0] * ym + _b[1] * yk;
			YnList.Add(deltaT * i, yn);
			i++;
			yk = ym;
			ym = yn;
			xm = xn;
			if (i < _count)
			{
				ModelRecursive(i, xm, ym, yk, deltaT);
			}
		}


	}
}
