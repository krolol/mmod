﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace MMod_lab3
{
	public partial class Form1 : Form
	{
		private Thread _thread;
		private Thread _addItemThread;
		private Thread _valueThread;
		private Thread _chartThread;
		private Queue _queue;
		private Service _service;
		private readonly List<Item> _notServiced;
		private readonly List<Item> _serviced;
		private readonly List<Item> _inService;
		private readonly List<double> pFinallyValues;
		private readonly List<double> pFinallyProbabilities;
		private List<double> _timeList;
		private List<double> _rejectionProbabilityList;
		private List<double> _commonCountList;
		private List<double> _inQueueList;
		private List<double> _inServiceList;
		private List<double> _servicedList;
		private List<double> _notServicedList;
		private readonly Mutex _mutex;
		private bool stop = false;
		private int count;
		public Form1()
		{
			InitializeComponent();
			_notServiced = new List<Item>();
			_serviced = new List<Item>();
			_inService = new List<Item>();
			pFinallyProbabilities = new List<double>();
			pFinallyValues = new List<double>();
			_mutex = new Mutex();
			count = 0;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (button1.Text == @"Imitate")
			{
				_thread = new Thread(Imitate);
				_thread.Start();
				_chartThread = new Thread(DataForChart);
				_chartThread.Start();
				button1.Text = @"Stop";
			}
			else
			{
				//MessageBox.Show(count.ToString());
				//MessageBox.Show(_notServiced.Count.ToString());
				_addItemThread.Abort();
				_thread.Abort();
				stop = true;
				Chart chart = new Chart(_timeList, _rejectionProbabilityList, _inServiceList, _inQueueList, _commonCountList, _servicedList, _notServicedList);
				chart.Show();
				//listBox1.Items.Clear();
				//listBox2.Items.Clear();
				//listBox3.Items.Clear();
				//_serviced.Clear();
				//_notServiced.Clear();
				//_queue.QueueItems.Clear();
				button1.Text = @"Imitate";
			}
		}

		private void Imitate()
		{
			int.TryParse(textBox2.Text, out int queueCapacity);
			int.TryParse(textBox5.Text, out int channelsCount);
			double.TryParse(textBox3.Text, out double requestsIntensity);
			double.TryParse(textBox4.Text, out double serviceIntensity);
			double.TryParse(textBox1.Text, out double v);
			Dictionary<string, double> args = new Dictionary<string, double>
			{
				{ "v", v },
				{ "requestsIntensity", requestsIntensity}
			};
			_queue = new Queue(queueCapacity);
			_service = new Service(channelsCount);
			_addItemThread = new Thread(AddItemInProcess);
			_addItemThread.Start(args);
			_valueThread = new Thread(DinamicValues);
			_valueThread.Start(queueCapacity + channelsCount);
			InitiateService(serviceIntensity);
			List<double> tFinalProbability = Theoretical.GetFinalsProbability(channelsCount, queueCapacity, requestsIntensity, serviceIntensity, v);
			double tAbsoluteBandWith = Theoretical.GetAbsoluteBandWith(tFinalProbability[tFinalProbability.Count - 1], requestsIntensity);
			double tAVGInSmo = Theoretical.GetAVGInSmo(channelsCount, queueCapacity, tFinalProbability);
			double tAVGInQueue = Theoretical.GetAVGInQueue(queueCapacity, channelsCount, tFinalProbability);
			double tAVGTimeInSmo = Theoretical.GetAVGTimeInSmo(tFinalProbability[tFinalProbability.Count - 1], serviceIntensity, v);
			double tAVGTimeInQueue = Theoretical.GetAVGTimeInQueue(tAVGTimeInSmo, v);
			double tAVGNotFreeChannels = Theoretical.GetAVGNotFreeChannels(tAbsoluteBandWith, serviceIntensity);
			BindData(tFinalProbability, tAbsoluteBandWith, tAVGInSmo, tAVGInQueue, tAVGTimeInSmo, tAVGTimeInQueue, tAVGNotFreeChannels);
		}

		private void AddItemInProcess(object args)
		{
			Dictionary<string, double> arguments = (Dictionary<string, double>)args;
			double v = arguments["v"];
			double requestsIntensity = arguments["requestsIntensity"];
			while (true)
			{
				Item item = new Item(Time.GetTime(v));
				count++;
				_mutex.WaitOne();
				if (_queue.QueueItems.Capacity != _queue.QueueItems.Count)
				{ 
					_queue.Add(item);
					Invoke((MethodInvoker)(() => listBox1.Items.Add(item)));
					Thread thread = new Thread(CheckElementInQueue);
					thread.Start(item);
					
				}
				else
				{
					_notServiced.Add(item);
					Invoke((MethodInvoker)(() => listBox2.Items.Add(item)));
				}
				_mutex.ReleaseMutex();
				Thread.Sleep((int)(1000 / requestsIntensity));
			}
		}

		private void InitiateService(double serviceIntensity)
		{
			while (_service.Channels.Capacity != _service.Channels.Count)
			{
				Channel channel = new Channel();
				_service.Channels.Add(channel);
				Thread channelThread = new Thread(WorkChannel);
				channelThread.Start(serviceIntensity);
			}
		}

		private void WorkChannel(object intensity)
		{
			while (true)
			{
				_mutex.WaitOne();
				if (_queue.QueueItems.Count != 0)
				{
					Item servicedItem = _queue.QueueItems[0];
					_queue.Remove(servicedItem);
					Invoke((MethodInvoker)(() => listBox1.Items.Remove(servicedItem)));
					_inService.Add(servicedItem);
					Invoke((MethodInvoker)(() => listBox5.Items.Add(servicedItem)));
					_mutex.ReleaseMutex();
					Thread.Sleep((int)(1000 / (double)intensity));
					_mutex.WaitOne();
					_serviced.Add(servicedItem);
					_inService.Remove(servicedItem);
					Invoke((MethodInvoker)(() => listBox3.Items.Add(servicedItem)));
					Invoke((MethodInvoker)(() => listBox5.Items.Remove(servicedItem)));
					_mutex.ReleaseMutex();
				}
				else
				{
					_mutex.ReleaseMutex();
				}
			}
		}

		private void CheckElementInQueue(object item)
		{
			Item itemForCheck = (Item)item;
			Thread.Sleep((int)(itemForCheck.TimeInQueue * 1000));
			_mutex.WaitOne();
			if (_queue.QueueItems.Contains(item))
			{
				_notServiced.Add(itemForCheck);
				_queue.Remove(itemForCheck);
				Invoke((MethodInvoker)(() => listBox1.Items.Remove(itemForCheck)));
				Invoke((MethodInvoker)(() => listBox2.Items.Add(itemForCheck)));
			}
			_mutex.ReleaseMutex();
		}

		public void BindData(List<double> tFinalProbability, double tAbsoluteBandWith, double tAVGInSmo, double tAVGInQueue, double tAVGTimeInSmo, double tAVGTimeInQueue, double tAVGNotFreeChannels)
		{
			foreach(var probability in tFinalProbability)
			{
				Invoke((MethodInvoker)(() => listBox4.Items.Add(probability)));
			}
			Invoke((MethodInvoker)(() => textBox7.Text = tFinalProbability[tFinalProbability.Count - 1].ToString()));
			Invoke((MethodInvoker)(() => textBox6.Text = tAbsoluteBandWith.ToString()));
			Invoke((MethodInvoker)(() => textBox8.Text = tAVGInSmo.ToString()));
			Invoke((MethodInvoker)(() => textBox9.Text = tAVGInQueue.ToString()));
			Invoke((MethodInvoker)(() => textBox10.Text = tAVGTimeInSmo.ToString()));
			Invoke((MethodInvoker)(() => textBox11.Text = tAVGTimeInQueue.ToString()));
			Invoke((MethodInvoker)(() => textBox12.Text = tAVGNotFreeChannels.ToString()));
		}

		private void DinamicValues(object sum)
		{
			for (int i = 0; i <= (int)sum; i++)
			{
				pFinallyValues.Add(0);
				pFinallyProbabilities.Add(0);
				Invoke((MethodInvoker)(() => listBox6.Items.Add(0)));
			}
			while (!stop)
			{
				_mutex.WaitOne();
				pFinallyValues[_inService.Count + _queue.QueueItems.Count]++;
				for (int i = 0; i <= (int)sum; i++)
				{
					pFinallyProbabilities[i] = pFinallyValues[i] / pFinallyValues.Sum();
					Invoke((MethodInvoker)(() => listBox6.Items[i] = pFinallyProbabilities[i]));
					Invoke((MethodInvoker)(() => textBox13.Text = Theoretical.GetAbsoluteBandWith(pFinallyProbabilities[(int)sum], Convert.ToDouble(textBox3.Text)).ToString()));
					Invoke((MethodInvoker)(() => textBox14.Text = (pFinallyProbabilities[(int)sum]).ToString()));
					Invoke((MethodInvoker)(() => textBox15.Text = Theoretical.GetAVGInSmo(Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox2.Text), pFinallyProbabilities).ToString()));
					Invoke((MethodInvoker)(() => textBox16.Text = Theoretical.GetAVGInQueue(Convert.ToInt32(textBox2.Text), Convert.ToInt32(textBox5.Text), pFinallyProbabilities).ToString()));
					Invoke((MethodInvoker)(() => textBox17.Text = Theoretical.GetAVGNotFreeChannels(Convert.ToDouble(textBox13.Text), Convert.ToDouble(textBox4.Text)).ToString()));
					Invoke((MethodInvoker)(() => textBox18.Text = Theoretical.GetAVGTimeInSmo(_notServiced.Count / (double)count, Convert.ToDouble(textBox4.Text), Time.GetTimeParameter()).ToString()));
					Invoke((MethodInvoker)(() => textBox19.Text = Theoretical.GetAVGTimeInQueue(Convert.ToDouble(textBox18.Text), Time.GetTimeParameter()).ToString()));
				}
				_mutex.ReleaseMutex();
			}
		}

		private void DataForChart()
		{
			Thread.Sleep(1000);
			_timeList = new List<double> {0};
			_rejectionProbabilityList = new List<double> {0};
			_inServiceList = new List<double>();
			_commonCountList = new List<double>();
			_inQueueList = new List<double>();
			_servicedList = new List<double>();
			_notServicedList = new List<double>();
			while (!stop)
			{
				_mutex.WaitOne();
				_timeList.Add(_timeList[_timeList.Count - 1] + 5);
				_rejectionProbabilityList.Add(pFinallyProbabilities[pFinallyProbabilities.Count - 1]);
				_commonCountList.Add(count);
				_inQueueList.Add(_queue.QueueItems.Count);
				_inServiceList.Add(_inService.Count);
				_servicedList.Add(_serviced.Count);
				_notServicedList.Add(_notServiced.Count);
				_mutex.ReleaseMutex();
				Thread.Sleep(5000);
			}
		}
	}
}
