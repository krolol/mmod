﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MathNet.Numerics.LinearAlgebra.Complex;

namespace lab2
{
	class Distributions
	{
		public static List<List<double>> Matrix;

		public double Geometric(double k, Dictionary<string, double> args)
		{
			double p = args["p"];
			double q = 1 - p;
			return Math.Pow(q, k) * p;
		}

		[SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
		public double Function(double k, Dictionary<string, double> args)
		{
			if (k == 1)
				return 0.3;
			if (k == 4)
				return 0.5;
			if (k == 5)
				return 0.2;
			return 0;
		}

		public double DoubleDsv(int i, int j)
		{
			Matrix = new List<List<double>>
			{
				new List<double>{0.01, 0.06, 0.03, 0.14, 0.03},
				new List<double>{0.02, 0.04, 0.14, 0.01, 0.02},
				new List<double>{0.04, 0.05, 0.03, 0.01, 0.05},
				new List<double>{0.02, 0.03, 0.04, 0.05, 0.05},
				new List<double>{0.02, 0.04, 0.03, 0.01, 0.03}
			};
			return Matrix[i][j]; 
		}

	}
}
