﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MMod_lab4.Сharacteristics;

namespace MMod_lab4
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			double.TryParse(textBox1.Text, out double count);
			double.TryParse(textBox2.Text, out double alpha);
			double.TryParse(textBox3.Text, out double deltaT);
			double.TryParse(textBox4.Text, out double countCor);
			Modeling modeling = new Modeling(count, alpha, deltaT);
			modeling.ModelRecursive(0, 0, 0, 0, deltaT);
			Dictionary<double, double> yn = modeling.YnList;
			double expectation = MathExp(modeling.YnList.Values.ToList());
			double dispersion = Dispersion(modeling.YnList.Values.ToList(), expectation);
			Dictionary<double, double> tCor = TCorrelation(expectation, dispersion, alpha, countCor, deltaT);
			Dictionary<double, double> pCor = PCorrelation(modeling.YnList.Values.ToList(), countCor, deltaT);
			Dictionary<double, double> portrait = Portrait(alpha, countCor, deltaT);
			Charts charts = new Charts(yn, pCor, tCor, portrait);
			charts.ShowDialog();
		}
	}
}
