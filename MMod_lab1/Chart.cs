﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace MMod_lab1
{
	public partial class Chart : Form
	{
		private readonly List<int> _countList;
		private readonly List<double> _numberList;
		private readonly double _expectation;
		private readonly double _dispersion;
		private readonly double _correctDispersion;
		private readonly double _leftExpectation;
		private readonly double _rightExpectation;
		private readonly double _leftDictionary;
		private readonly double _rightDictionary;
		public double Kolmogorov = -2;
		public double XiSquare = 0;
		public double TableXiSquare = 0;
		public Dictionary<string, double> Correllation = new Dictionary<string, double>();

		public Chart(List<double> numberList, List<int> countList, double expectation, double dispersion, double correctDispersion,
			double leftExpectation, double rightExpectation, double leftDictionary, double rightDictionary)
		{
			InitializeComponent();
			chart1.ChartAreas[0].AxisX.Title = "Generated numbers";
			chart1.ChartAreas[0].AxisY.Title = "Number of repetitions";
			_countList = countList;
			_numberList = numberList;
			_expectation = expectation;
			_dispersion = dispersion;
			_correctDispersion = correctDispersion;
			_leftExpectation = leftExpectation;
			_rightExpectation = rightExpectation;
			_leftDictionary = leftDictionary;
			_rightDictionary = rightDictionary;
		}

		private void OnLoad(object sender, EventArgs e)
		{
			BindChart();
		}

		private void BindChart()
		{
			chart1.Series[0].Points.DataBindXY(_numberList, _countList);
			label2.Text = _expectation.ToString(CultureInfo.InvariantCulture);
			label4.Text = _dispersion.ToString(CultureInfo.InvariantCulture);
			label6.Text = _correctDispersion.ToString(CultureInfo.InvariantCulture);
			label8.Text = _leftExpectation.ToString(CultureInfo.InvariantCulture);
			label10.Text = _rightExpectation.ToString(CultureInfo.InvariantCulture);
			label12.Text = _leftDictionary.ToString(CultureInfo.InvariantCulture);
			label14.Text = _rightDictionary.ToString(CultureInfo.InvariantCulture);
			if (Kolmogorov == -2)
			{
				label15.Text = "";
				label16.Text = "";
			}
			else
			{
				label16.Text = Kolmogorov.ToString(CultureInfo.InvariantCulture);
			}

			if (Correllation.Count != 0)
			{
				label19.Text = Math.Round(Correllation["1"],4).ToString(CultureInfo.InvariantCulture);
				label20.Text = Math.Round(Correllation["2"],4).ToString(CultureInfo.InvariantCulture);
			}
			else
			{
				label17.Text = "";
				label18.Text = "";
			}

			if (XiSquare != 0)
			{
				label22.Text = TableXiSquare.ToString(CultureInfo.InvariantCulture) + ">";
				label23.Text = XiSquare.ToString(CultureInfo.InvariantCulture);
			}
			else
			{
				label21.Text = "";
				label22.Text = "";
				label23.Text = "";
			}
		}
	}
}
