﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace lab2
{
	public partial class Form1 : Form
	{
		private readonly Distributions _distribution;
		private readonly Dictionary<string, double> _args;
		private readonly Dsv _dsv;
		private Marks _marks;
		private MarksDdsv _marksDDSV;
		private int _count = 100000;
		private int _countSection = 30;

		public Form1()
		{
			InitializeComponent();
			_distribution = new Distributions();
			_args = new Dictionary<string, double>();
			_dsv = new Dsv(_count, _countSection);
			_marksDDSV = new MarksDdsv();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (!_args.ContainsKey("p"))
			{
				_args.Add("p", 0.4);
			}

			List<double> dsv = _dsv.Generate(_distribution.Function, _args);
			listBox1.DataSource = dsv;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			List<double> probability = _dsv.GetProbability(_dsv.Numbers, _countSection);
			List<double> numbers = new List<double>();
			_marks = new Marks(_count, _dsv.Numbers);
			for (int i = 0; i < _countSection + 1; i++)
			{
				numbers.Add(i);
			}

			double expectation = _marks.MathExpect();
			double selectiveDispersion = _marks.SelectiveDispersion(expectation);
			double correctDispersion = _marks.CorrectedSelectiveDispersion(selectiveDispersion);
			Dictionary<string, double> intervalExpectation = _marks.IntervalForExpectation(expectation, correctDispersion);
			Dictionary<string, double> intervalDispersion = _marks.IntervalForDispersion(correctDispersion);
			double realXiSquare = Math.Round(Help.XiSquare(_dsv.Numbers, _count, _dsv.Fx), 2);
			double tableXiSquare =
				Math.Round(MathNet.Numerics.Distributions.ChiSquared.InvCDF((int) (_countSection / 3.0) - 3, 0.95), 2);
			Chart chart = new Chart(numbers, probability, expectation, selectiveDispersion, correctDispersion,
				intervalExpectation, intervalDispersion, realXiSquare, tableXiSquare);
			chart.Show();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			List<(double, double)> dDsv = _dsv.GenerateDoubleDsv(_distribution.DoubleDsv);
			listBox2.DataSource = dDsv;
		}

		private void button4_Click(object sender, EventArgs e)
		{
			List<List<double>> dDsvmatrix = _dsv.GetProbabilityForDDSV();
			Dictionary<string, double> expectation = _marksDDSV.GetExpectation(dDsvmatrix);
			Dictionary<string, double> dispersion = _marksDDSV.GetDispersion(dDsvmatrix, expectation["X"], expectation["Y"]);
			double corelation =
				_marksDDSV.GetCorelation(expectation["X"], expectation["Y"], dispersion["X"], dispersion["Y"], dDsvmatrix);
			Dictionary<string, double> intervalExpectationX = _marksDDSV.IntervalForExpectation(expectation["X"],
				_marksDDSV.CorrectedSelectiveDispersion(dispersion["X"], _count), _count);
			Dictionary<string, double> intervalExpectationY = _marksDDSV.IntervalForExpectation(expectation["Y"],
				_marksDDSV.CorrectedSelectiveDispersion(dispersion["Y"], _count), _count);
			Dictionary<string, double> intervalDispersionX =
				_marksDDSV.IntervalForDispersion(_marksDDSV.CorrectedSelectiveDispersion(dispersion["X"], _count), _count);
			Dictionary<string, double> intervalDispersionY =
				_marksDDSV.IntervalForDispersion(_marksDDSV.CorrectedSelectiveDispersion(dispersion["Y"], _count), _count);
			double realXiSquare = Math.Round(Help.XiSquare(_dsv.NotConvertedNumbersXY, _count, _dsv.Fxy));
			double tableXiSquare =
				Math.Round(MathNet.Numerics.Distributions.ChiSquared.InvCDF(8, 0.95), 2);
			Dictionary<string, double> theoreticalExpectation = _marksDDSV.GetExpectation(Distributions.Matrix);
			Dictionary<string, double> theoreticalDispersion = _marksDDSV.GetDispersion(Distributions.Matrix,
				theoreticalExpectation["X"], theoreticalExpectation["Y"]);
			Dictionary<string, List<double>> probabilities = Help.GetProbability(dDsvmatrix);
			Dictionary<string, List<double>> theoreticalProbabilities = Help.GetProbability(Distributions.Matrix);
			double zTestX = Help.ZExpectation(probabilities["X"], theoreticalProbabilities["X"], dispersion["X"], theoreticalDispersion["X"], _count);
			double zTestY = Help.ZExpectation(probabilities["Y"], theoreticalProbabilities["Y"], dispersion["Y"], theoreticalDispersion["Y"], _count);
			double fTestX = Help.FDispersion(dispersion["X"], theoreticalDispersion["X"]);
			double fTestY = Help.FDispersion(dispersion["Y"], theoreticalDispersion["Y"]);
			List<double> numbers = new List<double>();
			for (int i = 0; i < 5; i++)
				numbers.Add(i);

			ChartDDsv chart = new ChartDDsv(dDsvmatrix, expectation["X"], expectation["Y"], dispersion["X"], dispersion["Y"],
				corelation, intervalExpectationX, intervalExpectationY, intervalDispersionX, intervalDispersionY, realXiSquare,
				tableXiSquare, zTestX, zTestY, fTestX, fTestY);
			chart.Show();
			ChartDDsv2 chart2 = new ChartDDsv2(numbers, _marksDDSV.ProbabilityX, _marksDDSV.ProbabilityY);
			chart2.Show();
		}
	}
}